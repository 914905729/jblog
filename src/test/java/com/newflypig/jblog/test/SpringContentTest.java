package com.newflypig.jblog.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.newflypig.jblog.model.Article;
import com.newflypig.jblog.model.Pager;
import com.newflypig.jblog.model.Tag;
import com.newflypig.jblog.service.IArchiveService;
import com.newflypig.jblog.service.IArticleService;
import com.newflypig.jblog.service.IMenuService;
import com.newflypig.jblog.service.ISystemService;
import com.newflypig.jblog.service.ITagService;

@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试  
@ContextConfiguration(locations = "classpath:spring-*.xml")
public class SpringContentTest extends AbstractJUnit4SpringContextTests{
	
	@Resource(name = "articleService")
	private IArticleService articleService;
	
	@Resource(name = "archiveService")
	private IArchiveService archiveService;
	
	@Resource(name = "menuService")
	private IMenuService menuService;
	
	@Resource(name = "tagService")
	private ITagService tagService;
	
	@Resource(name = "systemService")
	private ISystemService systemService;
	
	@Test
	public void testFind(){
		
		System.out.println("==========================begin===========================");
		Pager<Article> pagerArticles = this.articleService.findPagerByMenu(1, "index");
		for(Article a : pagerArticles.getData()){
			System.out.print(a.getTitle() + "\t:");
			for(Tag t : a.getTags()){
				System.out.print(t.getTitle() + "\t" + t.getUrlName());
			}
			System.out.println();
		}
//		for(Menu menu : a.getMenus()){
//			System.out.println(menu.getTitle());
//		}
		
		//System.out.println(a.getTitle());
		
//		for(int i = 5; i < 10 ; i++){
//			Archive archive = new Archive();
//			archive.setTitle("2016年" + i + "月");
//			this.archiveService.save(archive);
//			System.out.println(archive.getArchiveId());
//		}
		
//		Archive archive = this.archiveService.findById(1);
//		System.out.println(archive.getCount());
//		List<Article> articleList = this.articleService.findByArchiveId(1);
//		for(Article article : articleList){
//			System.out.println(article.getTitle());
//		}
		
//		Menu menu = this.menuService.findById(1);
//		System.out.println(menu.getTitle());
//		for(Article article : menu.getArticles()){
//			System.out.println(article.getTitle());
//		}
		
//		for(Tag tag : this.tagService.findAll()){
//			System.out.println(tag.getTitle() + "\t" + tag.getCount());
//		}
		
		
//		System.out.println(this.systemService.checkLogin("newflypig", "821s5085"));
//		System.out.println(this.systemService.getValue(BlogSystem.KEY_BLOGTITLE));
		
		//this.archiveService.clearAll();
		//System.out.println(archive.getCount());
	}
}
