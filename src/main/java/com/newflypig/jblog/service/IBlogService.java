package com.newflypig.jblog.service;

import java.util.List;

/**
 * 与具体某种实体类无关的Service
 * 有关Blog的综合服务
 * @author newflydd@189.cn
 * 
 */
public interface IBlogService {
	/**
	 * 为搜索引擎sitemap提供整个系统的URL连接列表
	 * @return
	 */
	public List<String> getBlogUrls();
	
	/**
	 * 更新内存中的静态数据
	 */
	public void updateStatics();
}
