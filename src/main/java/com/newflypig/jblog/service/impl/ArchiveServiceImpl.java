package com.newflypig.jblog.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.newflypig.jblog.dao.IArchiveDAO;
import com.newflypig.jblog.dao.IBaseDAO;
import com.newflypig.jblog.model.Archive;
import com.newflypig.jblog.service.IArchiveService;

@Service("archiveService")
public class ArchiveServiceImpl extends BaseServiceImpl<Archive> implements IArchiveService {
	
	@Resource(name = "archiveDao")
	private IArchiveDAO archiveDao;
	
	@Override
	protected IBaseDAO<Archive> getDao() {
		return this.archiveDao;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void clearAll() {
		this.archiveDao.clearAll();
	}

}
