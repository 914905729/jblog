package com.newflypig.jblog.service;

import com.newflypig.jblog.model.BlogSystem;

public interface ISystemService extends IBaseService<BlogSystem>{
	/**
	 * 根据用户名密码，判断是否登录
	 * @param username
	 * @param pwd
	 * @return
	 */
	public boolean checkLogin(String username,String pwd);
	
	/**
	 * 根据key获取value
	 * @return
	 */
	public String getValue(String key);
	
	/**
	 * 更新键值对
	 * @param key
	 * @param value
	 */
	public void update(String key, String value);
	
	/**
	 * 修改密码
	 * @param oldPwd
	 * @param newPwd
	 * @return
	 */
	public int updatePwd(String oldPwd, String newPwd);

	public boolean checkGuest(String username, String password);
}
