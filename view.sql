/* 两个关键的视图定义，导出SQL时可能有其他无关信息影响阅读，这边给出了阅读级的SQL */
CREATE OR REPLACE VIEW jblog_tags_v 
AS select jt.id AS id,
          jt.title AS title,
          count(jat.jblog_tags_id) AS count 
from jblog_tags_t  jt
left join jblog_article_tags jat on jt.id = jat.jblog_tags_id
group by jt.id;

CREATE OR REPLACE VIEW jblog_archive_v 
AS select archive.id AS id,
          archive.title AS title,
          archive.url_name AS url_name,
          count(article.id) AS count 
from jblog_archive_t archive
left join (
     select * from jblog_article ja
     where ja.type = 0 
     and ja.state = 1
) as article on archive.id = article.archive_id
group by archive.id;